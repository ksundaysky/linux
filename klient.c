#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <getopt.h> 
#include <fcntl.h>
//#include <sys/siginfo.h>

//#define DATA "Uwielbiam LINUXA "


/*
 * Send a datagram to a receiver whose name is specified in the command
 * line arguments.  The form of the command line is <programname> <pathname>
 */

#define NAME "socket"
//#define NAME2 "socket2"

char bajt[9] = {'1', '0', '0', '0', '0', '0', '0', '1'};

int data=0;

int sock = 0;
void sigHandler( int sigNum, siginfo_t *si, void *pVoid)
{

    //union sigval sv;
    
    if(sigNum == SIGRTMIN+11)
    {
        sock = si->si_value.sival_int;

        //si->si_value.sival_int = 42;
    }

}

void sigFunc()
{
    struct sigaction sa;
    memset( &sa, '\0', sizeof(sa));
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;


    if( sigaction(SIGRTMIN+11, &sa, NULL) == -1 )
    {
        perror("Sigaction error! \n");
        exit(9);
    }
}

char* itoa1(int val, int base)
{ 
    static char buf[32] = {0}; 
    int i = 30; 
    for(; val && i ; --i, val /= base) 
        buf[i] = "0123456789abcdef"[val % base]; 
    return &buf[i+1]; 
}

int main(int argc,char* argv[])
{
    sigFunc();
    
    /*int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("opening datagram socket");
        exit(1);
    }*/

    int client_sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (client_sock < 0) 
    {
        perror("opening datagram socket");
        exit(1);
    }
    socklen_t  address_len;
    struct sockaddr_un name;
    name.sun_family = AF_UNIX;
    socklen_t  client_address_len;
    struct sockaddr_un client_name;
   //socklen_t  client_address_len = sizeof(client_name);
    
    client_name.sun_family = AF_UNIX;

    int fd1 = open("tab_ogl.txt", O_RDONLY);
    printf("Deskryptor pliku tab_ogl.txt %d\n", fd1);

    int pozycja = lseek(fd1, -108, 2);
    char buf_koniecpliku[108];
    pread(fd1, &buf_koniecpliku, 108, pozycja);
    printf("OdczytaĹem z tablicy adresow abstarkcyjnych %s\n", &buf_koniecpliku[1]);

    //strcpy(name.sun_path, buf_koniecpliku);
    strncpy(&name.sun_path[1], &buf_koniecpliku[1], sizeof(name.sun_path) - 1);
    printf("Name.sun_path: %c\n", name.sun_path[1]);
    name.sun_path[0] = 0;

    char socket_name[] = "ClientSo";
    strcpy(client_name.sun_path, socket_name);
    name.sun_path[13] = '\0';

    int pid = getpid();
    printf("pid: %d\n", pid);

    char* string_pid;
    string_pid = itoa1(pid, 10); 
    printf("pid(itoa): %s\n", string_pid);

    printf("Sizeof(string_pid): %ld\n", sizeof(string_pid));

    printf("Nazwa socketu: %s\n", client_name.sun_path);

    remove(client_name.sun_path);

    int client_bind = bind(client_sock, (struct sockaddr *) &client_name, sizeof(struct sockaddr_un));
    if (client_bind == -1) perror("Bind: ");
    //connect(sock, (struct sockaddr *)buf_koniecpliku, sizeof(buf_koniecpliku));
    if(sendto(client_sock, string_pid, sizeof(string_pid), 0, (struct sockaddr *) &name, sizeof(struct sockaddr_un)) < 0)
    {
        perror("sending datagram message");
    }

    char buf_socket[108];
    int odczyt;
   while(1)
    {
        odczyt = recvfrom(client_sock, buf_socket, 108, 0, NULL, NULL);
        if (odczyt == -1) perror("BĹad odczytu: ");
        printf("Przeczytano: %d\n", odczyt);
        printf("Bufor: %s\n", buf_socket);
    }

    unlink(buf_koniecpliku);
    unlink("socket");
    close(sock);
    return 0;
}